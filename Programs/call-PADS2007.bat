@echo off

echo.
echo            Copyright (C) 2015 QDevor
echo.
echo  Licensed under the GNU General Public License, Version 3.0 (the License);
echo  you may not use this file except in compliance with the License.
echo  You may obtain a copy of the License at
echo.
echo            http://www.gnu.org/licenses/gpl-3.0.html
echo.
echo Unless required by applicable law or agreed to in writing, software
echo distributed under the License is distributed on an AS IS BASIS,
echo WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
echo See the License for the specific language governing permissions and
echo limitations under the License.
echo.

cd /d %~dp0
title %~n0

:: setlocal enabledelayedexpansion
:----------------------------------------
setlocal enabledelayedexpansion
:----------------------------------------
:----------------------------------------
:----------------------------------------
set "USERNAME=anyDev"
set "USEREMAIL=qdevor@163.com"
:----------------------------------------
set "win_token=\"
set "unix_token=/"
:----------------------------------------
set "QDKE_PURE_PATH=C:/WINDOWS/system32;C:/WINDOWS;C:/WINDOWS/System32/Wbem"
set "QDKE_ORIGIN_PATH=%PATH%"
set "PATH=%QDKE_PURE_PATH%"
:----------------------------------------
if "x%QDK_ROOT%" == "x"  set "QDK_ROOT=D:/qdk"
set "QDKE_ROOT=D:/work_code/qdke"
if not exist %QDKE_ROOT% (
	set "QDKE_ROOT=D:/work_home/qdke_home/QDKe"
)
if "x%MXE_ROOT%" == "x"  set "MXE_ROOT=%QDKE_ROOT%/home/mxe"
:----------------------------------------
:----------------------------------------
set "DISK_DEVICE_LAST=C"
set "DISK_DEVICE_STR="
for /f "tokens=1* delims= " %%a in ('wmic LogicalDisk where "DriveType='3'" get DeviceID') do (
  if not "x%%a" == "xDeviceID" (
    set "DISK_DEVICE_STR=%%a!DISK_DEVICE_STR!"
  )
)
echo [debug] Detecting Disk DeviceID %DISK_DEVICE_STR%
set DISK_DEVICE_STR=!DISK_DEVICE_STR::=^

!
rem limit to 5GB
set "DISK_DEVICE_FREESPACE_MIN=5"
set "DISK_DEVICE_FREESPACE="
for /f "tokens=1* delims= " %%a in ("!DISK_DEVICE_STR!") do (
  set "DISK_DEVICE_LAST=%%a"
  for /f "tokens=1* delims= " %%m in ('wmic LogicalDisk where "DeviceID='!DISK_DEVICE_LAST!:'" get FreeSpace') do (
    set var=%%m
    set var=!var:~0,-3!
    set /a var=!var!/1024/1024 >nul 2>&1
    rem echo "!DISK_DEVICE_LAST! FreeSpace1=!var!GB"
    if not "x!var!" == "xFreeSpace" (
      if not "x!var!" == "x" (
        set "DISK_DEVICE_FREESPACE=!var!"
        rem echo "!DISK_DEVICE_LAST! FreeSpace3=!DISK_DEVICE_FREESPACE!GB"
        if !var! GTR !DISK_DEVICE_FREESPACE_MIN! goto :LABEL_GET_DISK_DEVICE_LAST
      )
    )
    rem echo "!DISK_DEVICE_LAST! FreeSpace2:!DISK_DEVICE_FREESPACE!GB"
  )
)
:LABEL_GET_DISK_DEVICE_LAST
echo [debug] Detecting Last Disk DeviceID %DISK_DEVICE_LAST%
echo [debug] Detecting Last Disk FreeSpace %DISK_DEVICE_FREESPACE%GB
if not !DISK_DEVICE_FREESPACE! GTR !DISK_DEVICE_FREESPACE_MIN! echo [debug] Detecting Last Disk FreeSpace small && pause && exit 1
:----------------------------------------
:----------------------------------------
:----------------------------------------
:: Base Envirements Setting
:----------------------------------------
:----------------------------------------

:----------------------------------------
if "x%MSYS_ROOT%" == "x"      set "MSYS_ROOT=%QDK_ROOT%/msys64"
if "x%MINGW32_ROOT%" == "x"   set "MINGW32_ROOT=%MSYS_ROOT%/mingw32"
if "x%MINGW64_ROOT%" == "x"   set "MINGW64_ROOT=%MSYS_ROOT%/mingw64"

if "x%MSYSTEM%" == "x"        set "MSYSTEM=MINGW32"

set "MINGW_ROOT=%MSYS_ROOT%/mingw64"
if "x%MSYSTEM%" == "xMINGW32" (
  set "MINGW_ROOT=%MSYS_ROOT%/mingw32"
)
:----------------------------------------
set "PATH=%MINGW_ROOT%/bin;%MSYS_ROOT%/usr/bin;%PATH%"
:----------------------------------------

:----------------------------------------
:----------------------------------------
:----------------------------------------
:     EDA Start
:----------------------------------------
echo [debug] [EDA] Starting.
:----------------------------------------
: Adding EDA PATH...
:----------------------------------------
set "EDA_ROOT=!QDK_ROOT!/eda"
set "EDA_NAME=PADS"
set "EDA_RUN=powerlogic"
set "EDA_VER=2007"
:----------------------------------------
set "EDA_PADS_ROOT=!EDA_ROOT!/PADS2007"
set "EDA_EDA_BIN_ROOT=!EDA_PADS_ROOT!/Programs"
:----------------------------------------
set "PATH=!EDA_EDA_BIN_ROOT:/=\!;!PATH!"
:----------------------------------------
: Setting EDA Env...
:----------------------------------------
set "MGLS_LICENSE_FILE=!EDA_PADS_ROOT!/Crack/LICENSE.TXT"
echo [EDA] [debug] Setting to MGLS_LICENSE_FILE=%MGLS_LICENSE_FILE%
:----------------------------------------
: Setting EDA Env... Done.
:----------------------------------------
: Creating a caller batch...
:----------------------------------------
echo [EDA] [debug] Creating a caller batch to !EDA_NAME! !EDA_VER!.
if not exist !EDA_EDA_BIN_ROOT!/call-!EDA_NAME!!EDA_VER!.bat (
  set var=!QDKE_ROOT!/etc/template
  pushd !var:/=\!
  cat batch-party-head.tpl ^
      batch-party-body-head.tpl ^
      batch-party-body-tail.tpl ^
      batch-party-env-eda-!EDA_NAME!.tpl ^
      batch-party-env-eda-run.tpl        ^
      batch-party-tail.tpl ^
  >!EDA_EDA_BIN_ROOT!/call-!EDA_NAME!!EDA_VER!.bat
  popd
)
:+++++++++++++++++++++++++++++++++++++
:----------------------------------------
: Creating a caller batch... Done.
:----------------------------------------
: Creating a desktop shortcut...
:----------------------------------------
echo [EDA] [debug] Creating a desktop shortcut to call !EDA_NAME! !EDA_VER!.
set "SCRIPT=!EDA_NAME!!EDA_VER!.vbs"
set "SHORTCUT_PATH=!EDA_EDA_BIN_ROOT!/call-!EDA_NAME!!EDA_VER!.bat"
set "SHORTCUT_NAME=call-!EDA_NAME!!EDA_VER!"
if exist !USERPROFILE!/Desktop/!SHORTCUT_NAME!.lnk (
  echo [debug] Detecting Exists - !USERPROFILE!/Desktop/!SHORTCUT_NAME!.lnk.
  goto :SKIP_CREATE_DESKTOP_SHORTCUT
)
echo [debug] SCRIPT=!SCRIPT!
echo [debug] SHORTCUT_PATH=!SHORTCUT_PATH!
echo [debug] SHORTCUT_NAME=!SHORTCUT_NAME!

echo set WshShell=CreateObject("WScript.Shell")                                  >!SCRIPT!
echo strDesktop = WshShell.SpecialFolders("Desktop")                            >>!SCRIPT!
echo set oMyShortCut= WshShell.CreateShortcut(strDesktop+"\!SHORTCUT_NAME!.lnk") >>!SCRIPT!
rem echo oMyShortCut.WindowStyle = 7  &&Minimized 0=Maximized  4=Normal >>!SCRIPT!
rem echo oMyShortcut.IconLocation = home()+"wizards\graphics\builder.ico" >>!SCRIPT!
echo oMyShortCut.TargetPath = "!SHORTCUT_PATH:/=\!"                             >>!SCRIPT!
rem echo oMyShortCut.Arguments = \'-c\'+\'\"\'+Home\(\)+\'config.fpw\'+\'\"\'   >>!SCRIPT!
rem echo oMyShortCut.WorkingDirectory = \"c:\\\" >>!SCRIPT!
echo oMyShortCut.Save >>!SCRIPT!

cscript //Nologo !SCRIPT:/=\!
del !SCRIPT!
:----------------------------------------
:SKIP_CREATE_DESKTOP_SHORTCUT
: Creating a desktop shortcut... Done.
:----------------------------------------
echo [debug] [EDA] Done.
:----------------------------------------
:     EDA END
:----------------------------------------
:+++++++++++++++++++++++++++++++++++++
echo [debug] [EDA] Running...
!EDA_RUN!.exe
:+++++++++++++++++++++++++++++++++++++

:----------------------------------------
:----------------------------------------
:----------------------------------------
:QDKE_EOF_
:++++++++++++++++++++++++++++++++++++++++
:: Doing Jobs Finish...
echo [QDKe] [debug] - We Are Doing Jobs... Finish.
:++++++++++++++++++++++++++++++++++++++++
title %~n0
:: setlocal disabledelayedexpansion
:----------------------------------------
setlocal disabledelayedexpansion
:----------------------------------------

PAUSE
EXIT