
@echo off
cd /d "%~dp0"

>NUL 2>&1 REG.exe query "HKU\S-1-5-19" || (
    ECHO SET UAC = CreateObject^("Shell.Application"^) > "%TEMP%\Getadmin.vbs"
    ECHO UAC.ShellExecute "%~f0", "%1", "", "runas", 1 >> "%TEMP%\Getadmin.vbs"
    "%TEMP%\Getadmin.vbs"
    DEL /f /q "%TEMP%\Getadmin.vbs" 2>NUL
    Exit /b
)

pushd %cd%
set "TOPDIR=%cd%"
cd ..
set "SDD_HOME=%cd%"
popd

echo Generating  LICENSE.TXT
call mentorkg -patch %SDD_HOME% -o %TOPDIR%/LICENSE.TXT
goto :__eof

echo Registering DaveNavigator.ocx
pushd %cd%
cd %SDD_HOME%/Programs
REGSVR32 /S DaveNavigator.ocx
popd

:__eof
pause

